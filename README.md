Guile Module Loader
-------------------

This is a simple module loader for if you are constantly referring to the same few
files and their functions and macros.

It should not really be done instead of having a module file for your project, but
rather this is meant to give an example at load time for your REPL.

I have only tested it on guile scheme so far as that is what I first developed it
for in the first place.

*NOTE:* Even though I have licensed this as GNU GPLv3, that does not make your project
GNU GPLv3 neccesarily, only the underlying path-file.scm and patches to that **directly**.
The usage of the interfaces does not mean you 

## How To Install

``` sh
cp $cloneDir/guilerc ~/.guile
cp $cloneDir/path-file.scm $PROG/scm/lib
```
or 

``` sh
ln -s $cloneDir/guilerc ~/.guile
#if you are not only going to use your .guile for this, reconsider linking guilerc in this fashion

ln -s $cloneDir/path-file.scm $PROG/scm/lib
```
note that you will have to edit the file if its got incorrect assumptions.
but you can make quick changes in your .guile like:
``` scheme 
(include "src/scheme/libraries/path-file.scm")
(define *programming-dir*
    (%programming-dir "src"))
(define *library-load-path*
    (%library-load-path "/scheme/libraries"))
(include-mod #t
             (list `(...)
                    ...))
```
and it will build the directory from `(getenv "HOME")`
