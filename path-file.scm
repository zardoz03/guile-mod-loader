;;; path-file.scm -*- mode: scheme; -*-
#|
This file is to be used to get the load path of various modules and such ,
if it is a library it will be using whatever you have installed to be the
library load path , otherwise it will be
hardcoded and given a github repo link

/usr/ /etc/ cannot really be written to,
also i do not want this to be a bash process or w.e.
|#
(use-modules (srfi srfi-1)
             (srfi srfi-71)#|for unlist|#
             #||#)

(define HOME (getenv "HOME"))
(define (%programming-dir ver)
  (string-append HOME "/" ver))
(define (%library-load-path ver)
  (string-append *programming-dir*
                 ver))

(define *programming-dir*
  (%programming-dir "programming")) ;;if not shadowed in say .guile
                                    ;;presume ~/programming
(define *library-load-path*
  (%library-load-path "/scm/lib"))  ;;presume ./scm/lib/
;;write your own %programming-dir functions though or place them in .guile or something

(define (pretty-print-mod-load
         n    #|number of modules, should be (length args)|#
         args #|names of modules loaded|#
         np   #|no print|#)
  "PROCEDURE IN GUILE-MOD-LOADER, DO NOT USE IN MAIN REPL SESSION, KEEP IT TO .GUILE
----------------------------------------------------------------------------------
sign :: (pretty-print-mod-load n args np)
n    := number of modules, should be (length args)
args := names of modules loaded, quasiquoted list of strings
np   := no print, changes nature of outputted text
----------------------------------------------------------------------------------"
  (let* ([ilst   (map (lambda (x) (if (>= (length x) 2)
                                   (apply string-append x)
                                   x))
                      args)]
         [lens   (map (lambda (x) (string-length (object->string x)))
                      ilst)]
         [maxlen (apply max lens)]
         [padlen (map (lambda (x) (- maxlen x))
                      lens)])
    (let ([sep (lambda (max)
                 (display (string-append (list->string
                                          (map (lambda _ #\-)
                                               (iota (+ 2 max))))
                                         "\n")))])
      (display (string-append ":: loaded "
                              (object->string n)
                              " modules\n"))
      (sep maxlen)
      (if np
          (display "")
          (let loop ([i 0])
            (if [>= i (length args)]
                (sep maxlen)
                (let* ([p (list-ref args i)]
                       [pads (list-ref padlen i)]
                       [padw (lambda (x)
                              (list->string
                               (map (lambda _ #\space)
                                    (iota x))))])
                  (display (apply string-append
                                  `(":- "
                                      ,@p
                                      ,(padw pads)
                                      " -: \n")))
                  (loop (1+ i)))))))))

(define (load-mod args)
  "PROCEDURE IN GUILE-MOD-LOADER, DO NOT USE IN MAIN REPL SESSION, KEEP IT TO .GUILE
----------------------------------------------------------------------------------
sign :: (load-mod args)
args := module paths (list of strings) to directly load

called after including the paths with include-mod,
only part of the system to directly `load'
----------------------------------------------------------------------------------"
  (let loop ([n 0])
    (if [>= n (length args)]
        #t
        [let ([N (list-ref args n)])
         (if [or (null? N)
                 (not (pair? N))]
             '[])
         (load (apply string-append
                      (if [= (length N) 1]
                          `(,*library-load-path* "/" ,@N)
                          N)))
         (loop (1+ n))])))

(define (include-mod
         np   #|no print|#
         args #|modules you want to load|#)
  "PROCEDURE IN GUILE-MOD-LOADER, DO NOT USE IN MAIN REPL SESSION, KEEP IT TO .GUILE
----------------------------------------------------------------------------------
sign :: (load-mod np args)
np   := no print, handed over to load-mod
args := module paths (list of strings) to directly load and include

ONLY FUNCTION TO DIRECTLY USE IN .GUILE
THE REST USED THRU ALTERATIONS ONTO GLOBAL VARIABLES/VARIABLES FOR THE SYSTEM

(include-mod #f (list
                  `(\"parser-study_utils.scm\")
                  `(\"saccharine.scm\")
                ;;`(,programming-dir \"/AOC/2021/utils.scm\")))
                ))
- a list with nothing but the quasiquoted string is interpreted to be only
  *library-load-path* and that string appended, anything else is interpreted as
  a hard part and _only_ printed as such, nothing changes but error or aesthetic
  changes before the REPL starts.

- #f prints the longer list form of the repl with all the paths
  #t only prints the number of modules loaded, would be preffered if you are
     loading many modules.

afaik this is only worth it for using within a .guile for things when you are
developing within the repl , and you dont neccessarily want to use it for
distributing software as it calls it rather bluntly with `load' and can be
imprudent to use.

but as long as you have a good knowledge of the inbuilt module system as this is
more of a work-around and cop-out wrt NixOS, and document yourself and your
dependencies clearly and lucidly, then you should be fiiine.
----------------------------------------------------------------------------------"
  (let loop ([n 0])
    (if [>= n (length args)]
        (pretty-print-mod-load n args np)
        [let ([N (list-ref args n)])
          (if [or (null? N)
                  (not (pair? N))]
              '[])
          (add-to-load-path (if [= (length N) 1]
                              *library-load-path*
                              (apply string-append
                                (reverse (cdr (reverse N))))))
          (loop (1+ n))]))
  (load-mod args))
